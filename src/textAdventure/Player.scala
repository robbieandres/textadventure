package textAdventure

import java.net.Socket
import java.io.PrintStream
import java.io.InputStream

//added ps to printDescription
class Player(
  private var nCurrentRoom: Room,
  private var nItems: List[Item],
  val ps: PrintStream,
  val is: InputStream,
  val name: String,
  private var position: Int) {

  nCurrentRoom.enterRoom(this)

  /*  val ps = new PrintStream(sock.getOutputStream())
  val is = sock.getInputStream() */

  //non blocking readLine
  private def readLine(): Option[String] = {
    if (is.available > 0) {
      val buf = new Array[Byte](is.available)
      is.read(buf)
      Some(new String(buf).trim)
    } else None
  }

  def move(dir: Int) {
    nCurrentRoom.getExit(dir) match {
      case Some(exit) =>
        Room.mapRooms(position).leaveRoom(this)
        position = exit.change
        nCurrentRoom = Room.mapRooms(exit.change)
        Room.mapRooms(position).enterRoom(this)
        nCurrentRoom.printDescription(ps)
      case None => ps.println("You arent typing right! Did you party too hard?")
    }
  }

  def items = nItems //given items

  def currentRoom = nCurrentRoom

  def addToInventory(item: Item): Unit = {
    nItems = item :: nItems
  }

  def printInventory(): Unit = {
    if (nItems != Nil) {
      ps.println("You have : " + nItems.map(_.name).mkString(","))
    } else ps.println("You got nothing in Inventory")
  }

  def getFromInventory(itemName: String): Option[Item] = {
    nItems.find(_.name == itemName) match {
      case Some(item) =>
        nItems = nItems.filter(_ != item)
        Some(item)
      case None =>
        ps.println("Sorry! You need to find items")
        None
    }
  }

  def printRoom() {
    nCurrentRoom.printDescription(ps)
  }

  def tellPlayer(s: String): Unit = {
    ps.println(s)
  }

  def update(): Unit = {
    readLine().foreach(input => {
      val (command, args) = { //grabs the command and items
        val space = input.indexOf(" ")
        if (space < 0) (input, "")
        else input.splitAt(space)
      }
      //  println(s":$command: :$args:")
      if (Player.commands.contains(command)) {
        Player.commands(command)(args.trim, this)
      }
    })
  }

  def checkRoom(curRoom: Room, lookingFor: String): Unit = {
    Room.mapRooms.find(_.name == lookingFor) match { //find room with matching name
      case Some(item) => //if there is a room with matching name
        ps.println(shortestPath(curRoom, item, List[Room]()).map(_.name))
      case None => println("Room looking for does not exist")
    }
    //check if the room searching for exists (compare to rooms in xml)
    //how do you do shortestPath in String
    // goes to rooms and loops through exits
  }

  def shortestPath(curRoom: Room, goal: Room, route: List[Room]): List[Room] = {
    var steps = 0
    if (curRoom == goal) {
      curRoom :: route
    } else {
      val pattern = for (Some(e) <- curRoom.exitList; if (!route.contains(Room.mapRooms(e.change)))) yield {
        shortestPath(Room.mapRooms(e.change), goal, curRoom :: route)
      }
      val sortPath = pattern.filter(_.nonEmpty).sortBy(_.length)
      if (!sortPath.isEmpty) sortPath(0)
      else Nil
    }
  }

}

object Player {

  
  val commands = TreeMap.apply[String, (String, Player) => Unit](
    "quit" -> ((args, p) =>
      sys.exit(0)),
    "east" -> ((args, p) =>
      p.move(0)),
    "west" -> ((args, p) =>
      p.move(1)),
    "north" -> ((args, p) =>
      p.move(2)),
    "south" -> ((args, p) =>
      p.move(3)),
    "up" -> ((args, p) =>
      p.move(4)),
    "down" -> ((args, p) =>
      p.move(5)),
    "look" -> ((args, p) =>
      p.printRoom()),
    "inventory" -> ((args, p) =>
      p.printInventory()),
    "get" -> ((args, p) => {
      p.currentRoom.getItem(args) match {
        case Some(item) =>
          p.addToInventory(item)
          println("You added " + item.name)
        case None =>
          println("No item to add!")
      }
    }),
    "drop" -> ((args, p) => {
      p.getFromInventory(args) match {
        case Some(item) =>
          p.currentRoom.dropItem(item)
          println("You dropped " + item.name)
        case None =>
          println("You have nothing in your Inventory")
      }
    }),
    "say" -> ((args, p) => {
      Room.mapRooms(p.position).tellRoom(p.name + " said: " + args /*commands.drop(4)*/ )
    }),
    "tell" -> ((args, p) => {
      val (userName, message) = {
        val space = args.indexOf(" ")
        if (space < 0) {
          (args, "")
        } else args.splitAt(space)
      }
      Main.playerList.find(_.name == userName) match {
        case Some(other) =>
          other.tellPlayer("Player " + p.name + " messaged you: " + message) /*commands.drop(4)*/
        case None => Nil
      }
    }),
    "shortPaths" -> ((args, p) => {
      p.checkRoom(p.nCurrentRoom, args)
    }))((s1,s2) => s1.compareTo(s2))
} 