package textAdventure

class Exit(val change:Int) {
  }


object Exit {

  def apply(dest:String):Option[Exit] = { 
    val n = dest.trim.toInt
    if(n >= 0) Some(new Exit(n))  //logic error
    else None 
    
  }
}