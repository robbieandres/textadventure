package textAdventure

import scala.xml._
import java.io.PrintStream


//needs to make a list of player that is mutable


class Room(val name:String, val description:String, private var items:List[Item], val exits:Array[Option[Exit]],private var players:List[Player]) {
  
    def printDescription(ps:PrintStream):Unit = {
      ps.println(name)
      ps.println(description)
      ps.println("You see:")
      items.foreach(i => ps.println(i.name))
      ps.println("Choices where to go:")
      for ((Some(e),dir) <- exits.zip(choices)) {
        ps.println(Room.mapRooms(e.change).name + " is "+dir)
      }
    }
    
    def checkRoom(roomName:String):Option[Room] = {
      ???
    }
    
    def exitList = exits
    
    def getItem(itemName:String):Option[Item] = { 
      items.find(_.name == itemName) match {   //how to grab item?
      case Some(item) =>
        items = items.filter(_ != item)
        Some(item)
        
      case None => None
      }
    }
     
    def dropItem(item:Item):Unit = items ::= item
    
    def getExit(dir:Int):Option[Exit] = {
      exits(dir)
    }
    
    val choices = List("east","west","north","south","up","down")
    
    def enterRoom(p:Player):Unit = {
      for (p2 <- players) {
        p2.ps.println(p.name+" has enterred the room!")
      }
      players ::= p
    }
    
    def leaveRoom(p:Player):Unit = {
      players = players.filter(_!=p)
      for (p2 <- players) {
        p2.ps.println(p.name+" has left the room!")
      }
    }
    
    def tellRoom(s:String): Unit = {
      for(p <- players) p.ps.println(s)
    }
}

object Room {
  
def apply(node: xml.Node):Room = {
    val name = (node \"@name").text.trim
    val desc = (node \"desc").text.trim
    val items = (node \"item").map(n => Item(n)).toList
    val exits = (node \"exit").text.split(",").map(s => Exit(s))
    new Room(name, desc,items,exits,Nil)
  }
 
  val mapRooms = readMap
  
  def readMap:Array[Room] = { 
     val xml = XML.loadFile("gameRoom.xml")
     (xml\ "room").map(n => Room(n)).toArray
  }
  println(readMap)
}