package textAdventure

import scala.io.StdIn.readLine
import java.net.ServerSocket
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.swing.event.EditDone
import scala.swing.TextField
import scala.swing.TextArea
import java.io.PrintStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.Socket
import java.io.ObjectInputStream
import java.io.BufferedInputStream
import java.io.ObjectOutputStream
import java.io.BufferedOutputStream

//not reading what is typed in
//needs to put actions happening in each room
//create multiple users using the game
// be careful when a player is being affected a race condition could occur

object Main {

  private var players:List[Player] = Nil
  
  def playerList = players
 
  def main(args: Array[String]) {   
    val host = "localhost"
    val port = 8799
    val ss = new ServerSocket(8799)
      Future {
        while(true) {
          val sock = ss.accept  
          val ps = new PrintStream(sock.getOutputStream())
          val is = sock.getInputStream()
          val b = Room.readMap
          ps.println("Let the Game Begin!")
          ps.println("\nType in your Name:")
          Future {
            while(is.available() < 1) 
            Thread.sleep(100)
            val buf = new Array[Byte](is.available)
            is.read(buf)
            val name = new String(buf).trim
            players ::= new Player(Room.mapRooms(0),Nil,ps,is, name, 0) 
            players.head.ps.println("\nWelcome to the Text Adventure Game!\n")
            Room.mapRooms(0)printDescription(players.head.ps) 
          }
        }
      }
    
    while(true) {
      Thread.sleep(100)
      players.foreach(_.update())  // for(p <- players) p.update
    }
  }
}

 /*     if ("quit".startsWith(input)) {
        println("Goodbye")
        sys.exit(0)
      } else if ("east".startsWith(input)) {
        player.move(0)
      } else if ("west".startsWith(input)) {
        player.move(1)
      } else if ("north".startsWith(input)) {
        player.move(2)
      } else if ("south".startsWith(input)) {
        player.move(3)
      } else if ("up".startsWith(input)) {
        player.move(4)
      } else if ("down".startsWith(input)) {
        player.move(5)
      } else if ("look".startsWith(input)) {
        player.printRoom()
      } else if ("inventory".startsWith(input)) {
        player.printInventory()
      } else if (input.startsWith("get ")) {
        val grabItem = input.drop(4).trim
        player.currentRoom.getItem(grabItem) match {
          case Some(item) =>
            player.addToInventory(item)
            println("You added " + item.name)
          case None =>
            println("No item to add!")
        }
      } else if (input.startsWith("drop ")) {
        val leaveItem = input.drop(5).trim
        player.getFromInventory(leaveItem) match {
          case Some(item) =>
            player.currentRoom.dropItem(item)
            println("You dropped " + item.name)
          case None =>
            println("You have nothing in your Inventory")
        }
      } */