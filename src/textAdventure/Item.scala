package textAdventure

import scala.xml._

class Item (val name: String, var loc:String ) {
 
  def matches (item:String):Boolean = {
    if (item == name) true
    else false
  }
}

object Item {

  def apply (node:xml.Node):Item = { 
    val name = (node\"@name").text
    val desc = node.text
    new Item(name, desc)
  }
}